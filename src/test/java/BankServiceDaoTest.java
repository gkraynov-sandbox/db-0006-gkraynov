import com.db.bank.service.BankServiceDao;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class BankServiceDaoTest {
    @Test
    public void shouldLoadBankFromDbWhenRequested() {
        BankServiceDao mockedDao = mock(BankServiceDao.class);
        mockedDao.loadBankFromDb();
        verify(mockedDao).loadBankFromDb();
    }
}
