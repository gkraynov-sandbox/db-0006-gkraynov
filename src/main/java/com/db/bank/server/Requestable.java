package com.db.bank.server;

public interface Requestable {
    String request(String request);
}
