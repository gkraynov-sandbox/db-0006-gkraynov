package com.db.bank.server;

import com.db.bank.exceptions.NetworkException;
import com.db.bank.exceptions.ServiceException;
import com.db.bank.models.Bank;
import com.db.bank.service.BankService;
import com.db.bank.testing.BankMocks;

public class BankServerApplication {
    private final BankService service;
    private final String feedFile;
    private Bank bank;
    private Requestable controller;

    public BankServerApplication(String feedFile) {
        service = new BankService();
        this.feedFile = feedFile;
    }

    public void start() {
        try {
            bank = service.readBankFromFeedFile(feedFile);
            controller = new BankController(service, bank);
            BankMocks.populateBank(bank);
            serve();
        } catch (ServiceException e) {
            System.out.println("Server failed to start");
            System.out.println(e.getMessage());
        }
    }

    private void serve() {
        try {
            SessionFactory factory = new SessionFactory("localhost", 8888);
            SessionPool pool = new SessionPool(factory, controller);
            pool.start();

        } catch (NetworkException e) {
            System.out.printf(e.toString());
        }
    }
}
