package com.db.bank.server;

import com.db.bank.exceptions.NetworkException;

import java.net.Socket;

public class SessionPool extends Thread {
    private final SessionFactory sessionFactory;
    private final Requestable controller;

    public SessionPool(SessionFactory sessionFactory, Requestable controller) {
        this.sessionFactory = sessionFactory;
        this.controller = controller;
    }

    public void serve() {
        try {
            while (!Thread.interrupted()) {
                Socket sock = sessionFactory.createSessionSocket();
                System.out.println("Client accepted");

                SessionJob job = new SessionJob(sock);
                Thread thread = new Thread(job);
                thread.setDaemon(true);
                thread.start();
            }
        } catch (NetworkException e) {
            System.out.println(e.getMessage());
            System.out.println(e.getCause().getMessage());
        }
    }

    @Override
    public void run() {
        serve();
    }

    class SessionJob implements Runnable {
        private final Socket socket;

        public SessionJob(Socket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {
            try (ProtocolWrapper wrapper = new ProtocolWrapper(socket, controller)) {
                wrapper.runSession();
                System.out.println("Client served :)");
            } catch (NetworkException e) {
                System.out.println("Failed to serve client: " + e.getMessage());
            }
        }
    }
}
