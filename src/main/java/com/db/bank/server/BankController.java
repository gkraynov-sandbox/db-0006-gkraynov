package com.db.bank.server;

import com.db.bank.exceptions.NoEnoughFundsException;
import com.db.bank.exceptions.ServiceException;
import com.db.bank.models.Account;
import com.db.bank.models.Bank;
import com.db.bank.models.Client;
import com.db.bank.models.Gender;
import com.db.bank.service.BankService;

public class BankController implements Requestable {
    private final BankService service;
    private final Bank bank;

    public BankController(BankService service, Bank bank) throws ServiceException {
        if (bank == null) {
            throw new ServiceException("Bank must not be null");
        }

        this.service = service;
        this.bank = bank;
    }

    @Override
    public String request(String request) {
        String response = null;
        synchronized (BankController.class) {
            if (request.startsWith("feed") && bank != null) {
                response = service.writeBankAsFeed(bank);
            }
            if (request.startsWith("withdraw")) {
                String[] args = request.split(" ");
                String name = args[1];
                String gender = args[2];
                String amount = args[3].trim();

                try {
                    //Хорошей практикой является в выражениях с equals ставить в левой части выражения константу.
                    //В данном случае, если gender == null, получим NullPointerException, если же
                    //выражение будет иметь вид "m".equals(gender) - получим false.
                    //И хорошим тоном является выносить константы в константные (public static final String GENDER_ARG например) переменные.
                    Client client = bank.findClient(name, (gender.equals("m")) ? Gender.MALE : Gender.FEMALE);
                    Account primaryAccount = client.getAccounts().iterator().next();
                    primaryAccount.withdraw(Double.parseDouble(amount));
                    response = "OK " + primaryAccount.maxWithdraw();
                } catch (NoEnoughFundsException e) {
                    response = "FAIL";
                }
            }
        }
        return response;
    }
}