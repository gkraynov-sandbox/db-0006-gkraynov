package com.db.bank.server;

import com.db.bank.exceptions.NetworkException;

import java.io.Closeable;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

public class SessionFactory implements Closeable {
    private ServerSocket bind;

    public SessionFactory(String host, int port) throws NetworkException {
        try
        {
            bind = new ServerSocket(port, 5, InetAddress.getByName(host));

        } catch (UnknownHostException e) {
            throw new NetworkException("Unknown host", e);
        } catch (IOException e) {
            throw new NetworkException("Failed to bind", e);
        }
    }

    public Socket createSessionSocket() throws NetworkException {
        try {
            return bind.accept();
        } catch (IOException e) {
            throw new NetworkException("Failed to accept new incoming connection", e);
        }
    }

    @Override
    public void close() throws NetworkException {
        try {
            bind.close();
        } catch (IOException e) {
            throw new NetworkException("Failed to close bind", e);
        }
    }
}
