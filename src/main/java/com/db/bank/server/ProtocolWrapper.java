package com.db.bank.server;

import com.db.bank.exceptions.NetworkException;

import java.io.*;
import java.net.Socket;

public class ProtocolWrapper implements Closeable {
    private Requestable controller;

    private Socket socket;
    private BufferedReader reader;
    private BufferedWriter writer;

    public ProtocolWrapper(Socket sock, Requestable controller) throws NetworkException {
        if (controller != null) {
            this.controller = controller;
        } else {
            throw new NullPointerException("Controller is required");
        }

        this.socket = sock;
        try {
            reader = new BufferedReader(new InputStreamReader(sock.getInputStream()));
            writer = new BufferedWriter(new OutputStreamWriter(sock.getOutputStream()));
        } catch (IOException e) {
            throw new NetworkException("Failed to open socket streams", e);
        }
    }

    public void runSession() throws NetworkException {
        try {
            String request = reader.readLine();
            if (request != null) {
                String response = controller.request(request);
                writer.write(response);
                writer.flush();
            } else {
                throw new NetworkException("No request");
            }
        } catch (IOException e) {
            throw new NetworkException("Connection dead", e);
        }
    }

    @Override
    public void close() throws NetworkException {
        if (!socket.isClosed()) {
            try {
                socket.close();
            } catch (IOException e) {
                throw new NetworkException("Failed to close socket", e);
            }
        }
    }
}
