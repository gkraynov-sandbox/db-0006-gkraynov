package com.db.bank.service;

import com.db.bank.models.Client;
import com.db.bank.service.BankPrinter;

import java.io.IOException;
import java.io.PrintWriter;

public class FileBankPrinter extends BankPrinter {
    private String fileName;
    private boolean consoleEcho;

    public FileBankPrinter(String fileName, boolean consoleEcho) {
        this.fileName = fileName;
        this.consoleEcho = consoleEcho;
    }

    @Override
    public void print(Iterable<Client> clients) {
        if (consoleEcho) {
            super.print(clients);
        }
        try (PrintWriter writer = new PrintWriter(fileName, "UTF-8")) {
            for (Client client : clients) {
                writer.println(client.toString());
            }
        } catch (IOException e) {
            System.out.println("IO Exception");
        }
    }
}
