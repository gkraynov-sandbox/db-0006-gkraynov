package com.db.bank.service;

import com.db.bank.exceptions.ServiceException;
import com.db.bank.models.Bank;
import com.db.bank.models.Client;
import com.db.bank.models.Account;
import com.db.bank.models.CheckingAccount;
import com.db.bank.models.Gender;
import com.db.bank.models.SavingsAccount;

import java.io.*;
import java.util.StringTokenizer;

public class BankService {
    /**
     * Native de-serialisation of bank
     * @param fileName File name of saved object
     * @return Bank
     * @throws ServiceException Once de-serialisation or IO were failed
     */
    public Bank readBank(String fileName) throws ServiceException {
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fileName))) {
            return (Bank) ois.readObject();
        } catch (IOException e) {
            throw new ServiceException("IO Exception during deserialization", e);
        } catch (Exception e) {
            throw new ServiceException("Malformed data to deserialize", e);
        }
    }

    /**
     * Native serialisation of bank
     * @param bank Bank
     * @param fileName File to save objects
     * @throws ServiceException Once serialisation or IO were failed
     */
    public void saveBank(Bank bank, String fileName) throws ServiceException {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fileName))) {
            oos.writeObject(bank);
        } catch (IOException e) {
            throw new ServiceException("IO Exception during serialization", e);
        }
    }

    /**
     * De-stringify bank from feed file
     * @param fileName Feed file name
     * @return Bank
     * @throws ServiceException Once there is problem with parsing or IO
     */
    public Bank readBankFromFeedFile(String fileName) throws ServiceException {
        final Bank bank = new Bank();

        try (BufferedReader feed = new BufferedReader(new InputStreamReader(new FileInputStream(fileName)))) {
            TokensEater eater = new TokensEater(bank);
            String line;
            while ((line = feed.readLine()) != null) {
                feedTheParser(eater, line);
            }
            eater.stop();
        } catch (IllegalArgumentException e) {
            throw new ServiceException("Malformed feed file", e);
        } catch (IOException e) {
            throw new ServiceException("Failed to open or read feed file", e);
        }

        return bank;
    }

    /**
     * De-stringify bank from feed-file formatted string
     * @param feed Feed string
     * @return Bank
     * @throws ServiceException Once there is problem with parsing
     */
    public Bank readBankFromFeed(String feed) throws ServiceException {
        final Bank bank = new Bank();

        try {
            TokensEater eater = new TokensEater(bank);
            feedTheParser(eater, feed);
            eater.stop();
        } catch (IllegalArgumentException e) {
            throw new ServiceException("Unable to parse feed", e);
        }

        return bank;
    }

    /**
     * Split string by tokens and feed the parser
     * @param eater Parser automate
     * @param feed Feed string
     */
    private void feedTheParser(TokensEater eater, String feed) {
        StringTokenizer tokens = new StringTokenizer(feed, "\n\r;=");
        while (tokens.hasMoreTokens()) {
            String token = tokens.nextToken();
            eater.feed(token);
        }
    }

    /**
     * Stringify bank in feed-file format
     * @param bank Bank to convert
     * @return String representation
     */
    public String writeBankAsFeed(Bank bank) {
        StringBuilder str = new StringBuilder();

        for (Client client : bank.getClients()) {
            for (Account account : client.getAccounts()) {
                AccountFields fields = new AccountFields(client, account);

                // Feed line example:
                // accounttype=c;balance=100;overdraft=50;name=John;gender=m;

                str.append("accounttype=");
                str.append(fields.isSavings ? "s" : "c");
                str.append(";balance=");
                str.append(fields.balance);
                str.append(";overdraft=");
                str.append(fields.overdraft);
                str.append(";name=");
                str.append(fields.name);
                str.append(";gender=");
                str.append(fields.gender == Gender.MALE ? "m" : "f");
                str.append(";\n");
            }
        }

        return str.toString();
    }


    /**
     * Finite automate to parse bank account fields from text feed
     */
    class TokensEater {
        private final Bank bank;

        private boolean stateKey = true;  // true - waiting for key, false - waiting for value
        private String lastKey = null;
        private AccountFields lastAccount = null;

        public TokensEater(Bank bank) {
            this.bank = bank;
        }

        /**
         * Push account into storage and switch back to a normal state
         */
        private void pushAccount() {
            if (lastAccount != null) {
                lastAccount.pushToBank(bank);
            }
            lastAccount = new AccountFields();
        }

        /**
         * Eat new token from key-waiting state
         * @param key New key to parse
         */
        private void eatKey(String key) {
            if (key.equals("accounttype")) {
                pushAccount();
            }
            lastKey = key;
        }

        /**
         * Eat new value from value-waiting state
         * @param value New value to parse
         */
        private void eatValue(String value) {
            if (lastKey == null || lastAccount == null) {
                throw new IllegalArgumentException("Deserialization error");
            }

            switch (lastKey) {
                case "accounttype":
                    if (value.equals("c")) {
                        lastAccount.isSavings = false;
                    } else if (value.equals("s")) {
                        lastAccount.isSavings = true;
                    } else {
                        throw new IllegalArgumentException();
                    }
                    break;

                case "balance":
                    lastAccount.balance = Double.parseDouble(value);
                    break;

                case "overdraft":
                    lastAccount.overdraft = Double.parseDouble(value);
                    break;

                case "name":
                    lastAccount.name = value;
                    break;

                case "gender":
                    if (value.equals("m")) {
                        lastAccount.gender = Gender.MALE;
                    } else if (value.equals("f")) {
                        lastAccount.gender = Gender.FEMALE;
                    } else {
                        throw new IllegalArgumentException();
                    }
                    break;

                default:
                    throw new IllegalArgumentException("Invalid key");
            }
        }

        /**
         * Feed new token to eat, state will be evaluated automatically
         * @param token New token to parse
         */
        public void feed(String token) {
            if (stateKey) {
                eatKey(token);
                stateKey = false;  // Expect value then
            } else {
                eatValue(token);
                stateKey = true;
            }
        }

        /**
         * Feed stop symbol in automate terminology
         */
        public void stop() {
            pushAccount();
        }
    };

    /**
     * Representation of fields for bank account
     */
    class AccountFields {
        boolean isSavings;
        double balance;
        double overdraft;

        String name;
        Gender gender;

        public AccountFields() {
        }

        public AccountFields(Client client, Account account) {
            if (account instanceof CheckingAccount) {
                // Checking account
                CheckingAccount checking = (CheckingAccount) account;
                isSavings = false;
                overdraft = checking.getOverdraft();
            } else {
                isSavings = true;
                overdraft = 0;
            }

            balance = account.getBalance();
            name = client.getName();
            gender = client.getGender();
        }

        public void pushToBank(Bank bank) {
            if (name == null || gender == null) {
                throw new IllegalArgumentException("Incomplete arguments");
            }
            Client client = bank.findClient(name, gender);

            Account account;
            if (isSavings) {
                account = new SavingsAccount(balance);
                if (overdraft != 0) {
                    throw new IllegalArgumentException("Savings account can not have ovedraft");
                }
            } else {
                account = new CheckingAccount(balance, overdraft);
            }

            if (client == null) {
                // Little overkill to help analyser
                if (isSavings || !(account instanceof CheckingAccount)) {
                    throw new IllegalArgumentException("Primary account must be checking");
                }
                bank.addClient(new Client(name, gender, (CheckingAccount) account));
            } else {
                client.addAccount(account);
            }
        }
    }
}