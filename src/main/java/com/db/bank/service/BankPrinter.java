package com.db.bank.service;

import com.db.bank.models.Client;

public class BankPrinter {
    public void print(Iterable<Client> clients) {
        for (Client client : clients) {
            System.out.println(client.toString());
        }
    }
}
