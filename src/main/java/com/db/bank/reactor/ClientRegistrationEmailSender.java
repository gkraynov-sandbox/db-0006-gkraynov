package com.db.bank.reactor;

import com.db.bank.models.Client;

public class ClientRegistrationEmailSender implements ClientRegistrationListener {
    private final EmailMessageFactory emailsFactory;

    public ClientRegistrationEmailSender() {
        emailsFactory = new EmailMessageFactory();
    }

    @Override
    public void onClientAdded(Client client) {
        EmailMessage email = emailsFactory.greetNewClient(client);
        EmailService.getInstance().enqueue(email);
    }
}
