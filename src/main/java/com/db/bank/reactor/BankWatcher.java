package com.db.bank.reactor;

import com.db.bank.models.Bank;
import com.db.bank.models.Client;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Multiplexer for ClientRegistrationListener
 */
public class BankWatcher implements ClientRegistrationListener {
    private final ArrayList<ClientRegistrationListener> registrationListeners;

    public BankWatcher(Bank bank) {
        registrationListeners = new ArrayList<ClientRegistrationListener>();
        invokeRegistrationListener(new ClientRegistrationDebugPrinter());
        bank.setRegistrationListener(this);
    }

    public void invokeRegistrationListener(ClientRegistrationListener listener) {
        assert listener != null;
        registrationListeners.add(listener);
    }

    public void revokeRegistrationListener(int index) {
        assert index >= 0 && index < registrationListeners.size();
        registrationListeners.remove(index);
    }

    @Override
    public void onClientAdded(Client client) {
        for (ClientRegistrationListener listener : registrationListeners) {
            listener.onClientAdded(client);
        }
    }

    /**
     * Debug printer
     */
    private class ClientRegistrationDebugPrinter implements ClientRegistrationListener {
        private DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

        @Override
        public void onClientAdded(Client client) {
            System.out.println(dateFormat.format(new Date()) + " " + client.getSalutation());
        }
    }
}
