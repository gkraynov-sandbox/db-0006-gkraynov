package com.db.bank.reactor;

import java.util.ArrayList;

/**
 * Non-blocking service to send E-Mails
 */
public class EmailService {
    private static volatile EmailService instance;
    private final EmailBlockingQueue queue;
    private final MailerThread mailer;

    public EmailService() {
        queue = new EmailBlockingQueue();
        mailer = new MailerThread();
        mailer.setDaemon(true);
        mailer.start();
    }

    public void enqueue(EmailMessage message) {
        queue.push(message);
    }

    private void send(EmailMessage message) throws InterruptedException {
        try {
            Thread.sleep(5000);

            // Dummy e-mail sender
            System.out.println("E-Mail sent to " + message.getRecipient());
            System.out.println(message.getBody());
            System.out.println("<EOM>");
            System.out.println();

        } catch (InterruptedException ir) {
            throw ir;
        }
    }

    /**
     * Lazy singleton pattern
     * @return Instance of EmailService
     */
    public static EmailService getInstance() {
        EmailService localInstance = instance;
        if (localInstance == null) {
            synchronized (EmailService.class) {
                localInstance = instance;
                if (localInstance == null) {
                    System.out.println("E-Mail service invoked");
                    localInstance = new EmailService();
                    instance = localInstance;
                }
            }
        }
        return localInstance;
    }

    class MailerThread extends Thread {
        @Override
        public void run() {
            while (true) {
                try {
                    EmailMessage message = queue.pop();
                    EmailService.this.send(message);
                } catch (InterruptedException ir) {
                    break;
                }
            }
        }
    }

    class EmailBlockingQueue {
        private Object monitor = new Object();
        private ArrayList<EmailMessage> queue = new ArrayList<>();

        public void push(EmailMessage message) {
            synchronized (monitor) {
                queue.add(message);
                monitor.notify();
            }
        }

        public EmailMessage pop() throws InterruptedException {
            EmailMessage message = null;
            synchronized (monitor) {
                while(queue.isEmpty()) {
                    monitor.wait();
                }
                message = queue.remove(0);
            }
            return message;
        }
    }
}
