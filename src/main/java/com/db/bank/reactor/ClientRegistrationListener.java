package com.db.bank.reactor;

import com.db.bank.models.Client;

public interface ClientRegistrationListener {
    void onClientAdded(Client client);
}
