package com.db.bank.reactor;

import com.db.bank.models.Client;

public class ClientRegistrationPrinter implements ClientRegistrationListener {
    @Override
    public void onClientAdded(Client client) {
        System.out.println(client.getSalutation());
    }
}
