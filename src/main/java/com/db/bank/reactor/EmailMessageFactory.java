package com.db.bank.reactor;

import com.db.bank.models.Client;

public class EmailMessageFactory {
    public EmailMessage greetNewClient(Client client) {
        String address = client.getName() + "@examplebank.com";
        String body = "Hi, " + client.getSalutation() + "!";

        return new EmailMessage(address, body);
    }
}
