package com.db.bank.reactor;

public class EmailMessage {
    private String recipient;
    private String body;

    public EmailMessage(String recipient, String body) {
        this.recipient = recipient;
        this.body = body;
    }

    public String getRecipient() {
        return recipient;
    }

    public String getBody() {
        return body;
    }
}
