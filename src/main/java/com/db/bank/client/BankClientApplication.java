package com.db.bank.client;

import com.db.bank.exceptions.ServiceException;
import com.db.bank.models.Bank;
import com.db.bank.models.Client;
import com.db.bank.models.Gender;
import com.db.bank.service.BankPrinter;
import com.db.bank.service.BankService;
import com.db.bank.service.FileBankPrinter;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

public class BankClientApplication {
    private final BankService service;
    private final String feedFile;
    private Bank bank;

    public BankClientApplication(String feedFile) {
        service = new BankService();
        this.feedFile = feedFile;
    }

    public void start() {
        try {
            if (feedFile == null) {
                getBankFromServer();
            } else {
                bank = service.readBankFromFeedFile(feedFile);
            }
            if (bank != null) {
                printBalance();

                if (feedFile == null) {
                    for (int i = 0; i < 50; i++) {
                        Thread.sleep(500);
                        smallWithdraw();
                    }
                }
            }
        } catch (ServiceException e) {
            System.out.printf("Service error");
            System.out.println(e.getMessage());
        } catch (InterruptedException ir) {
            // Exit
        }
    }

    private void getBankFromServer() throws ServiceException {
        bank = service.readBankFromFeed(query("feed"));
    }

    private void smallWithdraw() {
        Client client = bank.getClients().iterator().next();
        String gender = (client.getGender() == Gender.MALE) ? "m" : "f";
        String response = query("withdraw " + client.getName() + " " + gender + " 3");
        System.out.printf(response);
    }

    private String query(String request) {
        try {
            Socket sock = new Socket("localhost", 8888);

            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(sock.getOutputStream()));
            BufferedReader reader = new BufferedReader(new InputStreamReader(sock.getInputStream()));

            writer.write(request + "\r\n");
            writer.flush();

            String line;
            StringBuilder response = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                response.append(line);
                response.append("\n");
            }
            return response.toString();

        } catch (UnknownHostException e) {
            System.out.println("Connection failed");
        } catch (IOException e) {
            System.out.println("Failed to load bank from server");
        }

        return null;
    }

    private void printBalance() {
        BankPrinter printer = new FileBankPrinter("clients.txt", true);
        printer.print(bank.getClients());
    }
}
