package com.db.bank.exceptions;

import com.db.bank.exceptions.BankException;

public class NoEnoughFundsException extends BankException {
    private double amount;

    public NoEnoughFundsException(double amount) {
        this.amount = amount;
    }

    public NoEnoughFundsException(String message, double amount) {
        super(message);
        this.amount = amount;
    }

    public NoEnoughFundsException(String message, Throwable cause, double amount) {
        super(message, cause);
        this.amount = amount;
    }

    public double getAmount() {
        return amount;
    }
}
