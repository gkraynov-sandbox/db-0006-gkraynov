package com.db.bank.exceptions;

import com.db.bank.exceptions.NoEnoughFundsException;
import com.db.bank.models.Account;

public class OverdraftLimitExceededException extends NoEnoughFundsException {
    private Account account;

    public OverdraftLimitExceededException(double amount, Account account) {
        super(amount);
        this.account = account;
    }

    public OverdraftLimitExceededException(String message, double amount, Account account) {
        super(message, amount);
        this.account = account;
    }

    public OverdraftLimitExceededException(String message, Throwable cause, double amount, Account account) {
        super(message, cause, amount);
        this.account = account;
    }

    public Account getAccount() {
        return account;
    }
}
