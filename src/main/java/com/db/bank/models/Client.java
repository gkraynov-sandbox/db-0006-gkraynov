package com.db.bank.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

public class Client implements Cloneable, Serializable {
    private final String name;
    private final Gender gender;
    private ArrayList<Account> accounts;

    public Client(String name, Gender gender, CheckingAccount primaryAccount) {
        this.name = name;
        this.gender = gender;

        accounts = new ArrayList<>();
        accounts.add(primaryAccount);
    }

    public String getName() {
        return name;
    }

    public Gender getGender() {
        return gender;
    }

    public String getSalutation() {
        return getGender().salutationPrefix + " " + getName();
    }

    public void addAccount(Account account) {
        accounts.add(account);
    }

    public Iterable<Account> getAccounts() {
        return accounts;
    }

    @Override
    public boolean equals(Object another) {
        if (another == null) return false;
        if (!(another instanceof Client)) return false;

        Client such = (Client) another;
        if (!name.equals(such.name)) return false;
        if (!gender.equals(such.gender)) return false;
        if (!accounts.equals(such.accounts)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, gender, accounts);
    }

    @Override
    public Client clone() throws CloneNotSupportedException {
        return (Client) super.clone();
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append(getSalutation());
        str.append('\n');
        for (Account account : getAccounts()) {
            str.append("  " + account.toString());
            str.append(", Withdrawable: " + account.maxWithdraw() + "\n");
        }
        return str.toString();
    }
}
