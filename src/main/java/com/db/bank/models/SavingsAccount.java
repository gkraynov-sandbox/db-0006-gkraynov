package com.db.bank.models;

import com.db.bank.models.AbstractAccount;

import java.io.Serializable;

public class SavingsAccount extends AbstractAccount implements Serializable {
    @Override
    public double maxWithdraw() {
        return balance;
    }

    @Override
    public String toString() {
        return String.format("Savings account, Balance: %f", balance);
    }

    public SavingsAccount(double balance) {
        super(balance);
    }
}
