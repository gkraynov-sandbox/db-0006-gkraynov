package com.db.bank.models;

import com.db.bank.exceptions.NoEnoughFundsException;

import java.io.Serializable;
import java.util.Objects;

public abstract class AbstractAccount implements Account, Serializable {
    protected double balance;

    @Override
    public double getBalance() {
        return balance;
    }

    @Override
    public long decimalValue() {
        return Math.round(getBalance());
    }

    @Override
    public void deposit(double value) {
        assert value >= 0;
        balance += value;
    }

    @Override
    public void withdraw(double value) throws NoEnoughFundsException {
        if (value > balance) {
            throw new NoEnoughFundsException(value - balance);
        }
        balance -= value;
    }

    public AbstractAccount(double balance) {
        this.balance = balance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AbstractAccount that = (AbstractAccount) o;
        return Objects.equals(balance, that.balance);
    }

    @Override
    public int hashCode() {
        return Objects.hash(balance);
    }
}
