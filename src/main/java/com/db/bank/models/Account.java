package com.db.bank.models;

import com.db.bank.exceptions.NoEnoughFundsException;

public interface Account {
    double getBalance();
    long decimalValue();
    void deposit(double value);
    void withdraw(double value) throws NoEnoughFundsException;
    double maxWithdraw();
}
