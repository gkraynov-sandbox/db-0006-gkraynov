package com.db.bank.models;

public enum Gender {
    MALE("Mr."),
    FEMALE("Ms."),
    OTHER("Mr/s.");

    public final String salutationPrefix;

    Gender(String prefix) {
        this.salutationPrefix = prefix;
    }
}
