package com.db.bank.models;

import com.db.bank.reactor.ClientRegistrationListener;

import java.io.Serializable;
import java.util.ArrayList;

public class Bank implements Serializable {
    private ArrayList<Client> clients = new ArrayList<Client>();
    transient private ClientRegistrationListener registrationListener;

    public void setRegistrationListener(ClientRegistrationListener registrationListener) {
        this.registrationListener = registrationListener;
    }

    public Iterable<Client> getClients() {
        return clients;
    }

    public void addClient(Client client) {
        clients.add(client);
        if (registrationListener != null) {
            registrationListener.onClientAdded(client);
        }
    }

    public Client findClient(String name, Gender gender) {
        for (Client client : clients) {
            if (client.getName().equals(name) && client.getGender() == gender) {
                return client;
            }
        }
        return null;
    }
}
