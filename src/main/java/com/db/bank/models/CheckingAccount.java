package com.db.bank.models;

import com.db.bank.exceptions.OverdraftLimitExceededException;

import java.io.Serializable;
import java.util.Objects;

public class CheckingAccount extends AbstractAccount implements Serializable {
    protected double overdraft;

    public CheckingAccount(double balance, double overdraft) {
        super(balance);
        assert -balance <= overdraft;
        this.overdraft = overdraft;
    }

    public double getOverdraft() {
        return overdraft;
    }

    @Override
    public void withdraw(double value) throws OverdraftLimitExceededException {
        if (value > (balance + overdraft)) {
            throw new OverdraftLimitExceededException(value - balance - overdraft, this);
        }
        balance -= value;
    }

    @Override
    public double maxWithdraw() {
        return balance + overdraft;
    }

    @Override
    public String toString() {
        return String.format("Checking account, Balance: %f, Overdraft: %f", balance, overdraft);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        CheckingAccount that = (CheckingAccount) o;
        return Objects.equals(overdraft, that.overdraft);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), overdraft);
    }
}
