package com.db.bank.testing;

import com.db.bank.exceptions.NoEnoughFundsException;
import com.db.bank.exceptions.OverdraftLimitExceededException;
import com.db.bank.models.*;
import com.db.bank.reactor.BankWatcher;
import com.db.bank.reactor.ClientRegistrationEmailSender;
import com.db.bank.reactor.ClientRegistrationPrinter;

public class BankMocks {
    public static void populateBank(Bank bank) {
        BankWatcher watcher = new BankWatcher(bank);
        watcher.invokeRegistrationListener(new ClientRegistrationPrinter());

        bank.addClient(new Client("Alex", Gender.MALE, new CheckingAccount(100, 3000)));
        bank.addClient(new Client("Jessica", Gender.FEMALE, new CheckingAccount(200, 3000)));
        bank.addClient(new Client("Andy", Gender.MALE, new CheckingAccount(300, 3000)));
        bank.addClient(new Client("Greg", Gender.MALE, new CheckingAccount(400, 3000)));

        watcher.invokeRegistrationListener(new ClientRegistrationEmailSender());

        bank.addClient(new Client("Linda", Gender.FEMALE, new CheckingAccount(500, 3000)));
        bank.addClient(new Client("Conchita", Gender.OTHER, new CheckingAccount(600, 3000)));

        watcher.revokeRegistrationListener(0);

        bank.addClient(new Client("Melinda", Gender.FEMALE, new CheckingAccount(700, 3000)));
        bank.addClient(new Client("Chris", Gender.MALE, new CheckingAccount(800, 3000)));
        bank.addClient(new Client("Matthew", Gender.MALE, new CheckingAccount(900, 3000)));
        bank.addClient(new Client("Katya", Gender.FEMALE, new CheckingAccount(50, 300)));

        modifyBank(bank);
    }

    public static void modifyBank(Bank bank) {
        try {
            Iterable<Client> clients = bank.getClients();
            for (Client client : clients) {
                for (Account account : client.getAccounts()) {
                    account.deposit(20);
                    account.withdraw(2);
                }
            }

            clients.iterator().next().getAccounts().iterator().next().withdraw(2000);
            clients.iterator().next().getAccounts().iterator().next().withdraw(2000000);
        } catch (OverdraftLimitExceededException e) {
            System.out.println("Overdraft exception");
            System.out.println(e.getAccount().toString());
        } catch (NoEnoughFundsException e) {
            System.out.println(e.toString());
        }
    }
}
