package com.db.bank;

import com.db.bank.client.BankClientApplication;
import com.db.bank.server.BankServerApplication;

public class BankApplication {
    public static void main(String... args) {
        String feedFile = null;
        Boolean isServer = false;

        try {
            if (args.length >= 2 && args[0].equals("-loadfeed")) {
                feedFile = args[1];
                if (args.length == 3 && args[2].equals("-server")) {
                    isServer = true;
                }
            } else if (args.length == 1 && args[0].equals("-server")) {
                isServer = true;
                feedFile = "feed.txt";
            } else if (args.length != 0) {
                throw new IllegalArgumentException("Invalid cmd args");
            }

            if (isServer) {
                BankServerApplication server = new BankServerApplication(feedFile);
                server.start();
            } else {
                BankClientApplication client = new BankClientApplication(feedFile);
                client.start();
            }
        } catch (IllegalArgumentException e) {
            System.out.println(e.toString());
        }
    }
}